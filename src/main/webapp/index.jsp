<%-- 
    Document   : index
    Created on : 06-07-2021, 22:03:36
    Author     : AOrellana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ejercicio03
        </h1>
        <h1>Listar ventade autos -GET- https://ejercicio003.herokuapp.com/api/stock</h1>
        <h1>Listar por ID -GET- https://ejercicio003.herokuapp.com/api/stock/{TOYOTA}</h1>
        <h1>CREAR STOCK -POST- https://ejercicio003.herokuapp.com/api/stock</h1>
        <h1>METODO ACTUALIZAR -PUT-  https://ejercicio003.herokuapp.com/api/stock</h1>
        <h1>ELIMINAR POR ID -DELETE-  https://ejercicio003.herokuapp.com/api/stock/{NISSAN}</h1>
        
    </body>
</html>
