/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.ejercicio03.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author AOrellana
 */
@Entity
@Table(name = "ventaautos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ventaautos.findAll", query = "SELECT v FROM Ventaautos v"),
    @NamedQuery(name = "Ventaautos.findByModelo", query = "SELECT v FROM Ventaautos v WHERE v.modelo = :modelo"),
    @NamedQuery(name = "Ventaautos.findByTipoauto", query = "SELECT v FROM Ventaautos v WHERE v.tipoauto = :tipoauto"),
    @NamedQuery(name = "Ventaautos.findByPrecio", query = "SELECT v FROM Ventaautos v WHERE v.precio = :precio"),
    @NamedQuery(name = "Ventaautos.findByFecha", query = "SELECT v FROM Ventaautos v WHERE v.fecha = :fecha"),
    @NamedQuery(name = "Ventaautos.findByPuertas", query = "SELECT v FROM Ventaautos v WHERE v.puertas = :puertas")})
public class Ventaautos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "modelo")
    private String modelo;
    @Size(max = 2147483647)
    @Column(name = "tipoauto")
    private String tipoauto;
    @Size(max = 2147483647)
    @Column(name = "precio")
    private String precio;
    @Size(max = 2147483647)
    @Column(name = "fecha")
    private String fecha;
    @Size(max = 2147483647)
    @Column(name = "puertas")
    private String puertas;

    public Ventaautos() {
    }

    public Ventaautos(String modelo) {
        this.modelo = modelo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipoauto() {
        return tipoauto;
    }

    public void setTipoauto(String tipoauto) {
        this.tipoauto = tipoauto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPuertas() {
        return puertas;
    }

    public void setPuertas(String puertas) {
        this.puertas = puertas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modelo != null ? modelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ventaautos)) {
            return false;
        }
        Ventaautos other = (Ventaautos) object;
        if ((this.modelo == null && other.modelo != null) || (this.modelo != null && !this.modelo.equals(other.modelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.ejercicio03.entity.Ventaautos[ modelo=" + modelo + " ]";
    }
    
}
