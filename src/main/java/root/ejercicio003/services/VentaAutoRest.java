/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.ejercicio003.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.ejercicio03.dao.VentaautosJpaController;
import root.ejercicio03.dao.exceptions.NonexistentEntityException;
import root.ejercicio03.entity.Ventaautos;

/**
 *
 * @author AOrellana
 */

@Path("/stock")
public class VentaAutoRest {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarVentaAutos(){
    
    VentaautosJpaController dao = new VentaautosJpaController();
    
   List<Ventaautos> ventaautos = dao.findVentaautosEntities();
    
   return Response.ok(200).entity(ventaautos).build();
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Ventaautos ventaautos){
        
         VentaautosJpaController dao = new VentaautosJpaController();
        try {
            dao.create(ventaautos);
        } catch (Exception ex) {
            Logger.getLogger(VentaAutoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    return Response.ok(200).entity(ventaautos).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Ventaautos ventaautos) {
        VentaautosJpaController dao = new VentaautosJpaController();

        try {
            dao.edit(ventaautos);
        } catch (Exception ex) {
            Logger.getLogger(VentaAutoRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(ventaautos).build();

    }
    
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar){
    
        VentaautosJpaController dao = new VentaautosJpaController();
    
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(VentaAutoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Se ha eliminado de la base de datos").build();
    
    
    }
    
    
    @GET
    @Path("/{idconsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultaId(@PathParam("idconsultar") String idconsultar) {

        VentaautosJpaController dao = new VentaautosJpaController();
        Ventaautos ventaautos = dao.findVentaautos(idconsultar);

        return Response.ok(200).entity(ventaautos).build();

    }
}



